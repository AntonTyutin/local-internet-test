<?php
/**
 * Этот файл создан в рамках тестового задания Local Internet
 * @author Anton Tyutin <anton@tyutin.ru>
 * @license MIT
 */

namespace LocalInternet\Chess\Board;


use LocalInternet\Chess\EventDispatcher as Event;
use LocalInternet\Chess\Exception as Exception;
use LocalInternet\Chess\Piece\AbstractPiece;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Шахматная доска
 * @package LocalInternet\Chess\Board
 */
class Board
{
    /**
     * Ширина доски
     * @var int
     */
    private $width;

    /**
     * Высота доски
     * @var int
     */
    private $height;

    /**
     * Расстановка фигур на доске
     * @var Position[]
     */
    private $positions;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    public function __construct(int $width, int $height, EventDispatcherInterface $eventDispatcher)
    {
        $this->width = $width;
        $this->height = $height;
        $this->eventDispatcher = $eventDispatcher;
        $this->positions = [];
    }

    /**
     * Положения фигур на доске
     * @return Position[]
     */
    public function getPositions(): array
    {
        return array_values($this->positions);
    }

    /**
     * Ширина доски
     * @return int
     */
    public function getWidth(): int
    {
        return $this->width;
    }

    /**
     * Высота доски
     * @return int
     */
    public function getHeight(): int
    {
        return $this->height;
    }

    /**
     * Добавление фигуры на доску
     * @param AbstractPiece $piece
     * @param string $coordinates
     * @throws Exception\CoordinatesFormatException При некорректно указанных координатах
     * @throws Exception\PositionConflictException Если целевые координаты заняты другой фигурой
     */
    public function addPiece(AbstractPiece $piece, string $coordinates)
    {
        if (!$this->checkCoordinatesValid($coordinates)) {
            throw new Exception\CoordinatesFormatException();
        }
        if ($this->hasPieceAt($coordinates)) {
            throw new Exception\PositionConflictException($this, $piece, $this->positions[$coordinates]);
        }

        $this->positions[$coordinates] = new Position($this, $piece, $coordinates);
        $this->eventDispatcher->dispatch(
            'chess_board.position.add',
            new Event\PositionAddEvent($this->positions[$coordinates])
        );
    }

    /**
     * Удаление фигуры с доски
     * @param string $coordinates
     * @return Position
     * @throws Exception\CoordinatesFormatException При некорректно указанных координатах
     * @throws Exception\EmptyCellException Если по указанным координатам нет фигуры
     */
    public function removePiece(string $coordinates): Position
    {
        if (!$this->checkCoordinatesValid($coordinates)) {
            throw new Exception\CoordinatesFormatException();
        }
        if (!$this->hasPieceAt($coordinates)) {
            throw new Exception\EmptyCellException($this, $coordinates);
        }

        $position = $this->positions[$coordinates];
        unset($this->positions[$coordinates]);

        $this->eventDispatcher->dispatch('chess_board.position.remove', new Event\PositionRemoveEvent($position));

        return $position;
    }

    /**
     * Перемещение фигуры по доске
     * @param string $from
     * @param string $to
     * @return Position
     * @throws Exception\CoordinatesFormatException При некорректно указанных координатах
     * @throws Exception\EmptyCellException Если по исходным координатам нет фигуры
     * @throws Exception\InvalidMoveException Если фигура не может так ходить
     * @throws Exception\PositionConflictException Если целевые координаты заняты другой фигурой
     */
    public function movePiece(string $from, string $to): Position
    {
        $piece = $this->positions[$from]->getPiece();

        if (!$this->checkCoordinatesValid($from) || !$this->checkCoordinatesValid($to)) {
            throw new Exception\CoordinatesFormatException();
        }
        if (!isset($this->positions[$from])) {
            throw new Exception\EmptyCellException($this, $from);
        }
        if (isset($this->positions[$to])) {
            throw new Exception\PositionConflictException($this, $piece, $to);
        }
        if (!$piece->isAbleToMove($from, $to)) {
            throw new Exception\InvalidMoveException($this, $piece, $from, $to);
        }

        $fromPosition = $this->positions[$from];
        unset($this->positions[$from]);
        $this->positions[$to] = new Position($this, $fromPosition->getPiece(), $to);

        $this->eventDispatcher->dispatch(
            'chess_board.position.move',
            new Event\PositionMoveEvent($this->positions[$to], $fromPosition)
        );

        return $this->positions[$to];
    }

    /**
     * Получение позиции фигуры по указанным координатам
     * @param string $coordinates
     * @return Position
     * @throws Exception\EmptyCellException
     */
    public function getPosition(string $coordinates): Position
    {
        if (!$this->hasPieceAt($coordinates)) {
            throw new Exception\EmptyCellException($this, $coordinates);
        }

        return $this->positions[$coordinates];
    }

    /**
     * Проверка наличия фигуры по указанным координатам
     * @param string $coordinates
     * @return bool
     * @throws Exception\CoordinatesFormatException При некорректно указанных координатах
     */
    public function hasPieceAt(string $coordinates): bool
    {
        if (!$this->checkCoordinatesValid($coordinates)) {
            throw new Exception\CoordinatesFormatException();
        }

        return isset($this->positions[$coordinates]);
    }

    /**
     * Проверка корректности координат для данной доски
     * @param string $coordinates
     * @return bool
     */
    public function checkCoordinatesValid(string $coordinates): bool
    {
        if (!CoordinatesConverter::checkString($coordinates)) {
            return false;
        }

        list($col, $row) = CoordinatesConverter::fromString($coordinates);

        return $col <= $this->width and $row <= $this->height;
    }
}
