<?php
/**
 * Этот файл создан в рамках тестового задания Local Internet
 * @author Anton Tyutin <anton@tyutin.ru>
 * @license MIT
 */

namespace LocalInternet\Chess\Board;


use LocalInternet\Chess\Exception;

class CoordinatesConverter
{
    const COORDINATES_STRING_REGEXP = '/^([a-z]+)([1-9]\d*)$/';

    /**
     * Преобразование координат [$col, $row] в строковые "d5"
     * @param array $coordinates
     * @return string
     * @throws Exception\CoordinatesFormatException При неправильно указанных координатах
     */
    public static function toString(array $coordinates): string
    {
        if (count($coordinates) != 2) {
            throw new Exception\CoordinatesFormatException(
                'Некорректный формат координат для преобразования. Ожидался массив [$col, $row]'
            );
        }
        list($col, $row) = $coordinates;
        if (!is_int($col) || !is_int($row)) {
            throw new Exception\CoordinatesFormatException('Координаты должны быть заданы целыми числами');
        }
        if ($col < 1 || $row < 1) {
            throw new Exception\CoordinatesFormatException(
                ($col < 1 ? 'Столбец' : 'Строка') . ' не может быть меньше 1'
            );
        }

        $colString = '';
        do {
            $letterIdx = ($col - 1) % 26;
            $colString = chr(0x61 + $letterIdx) . $colString;
            $col /= 26;
        } while ($col > 1);

        return $colString . $row;
    }

    /**
     * Преобразование координат "d5" в массив [$col, $row]
     * @param string $coordinates
     * @return array
     * @throws Exception\CoordinatesFormatException
     */
    public static function fromString(string $coordinates): array
    {
        if (!preg_match(self::COORDINATES_STRING_REGEXP, $coordinates, $matches)) {
            throw new Exception\CoordinatesFormatException('Не верно указаны координаты в форме строки');
        }

        $row = (int)$matches[2];
        $col = 0;

        foreach (str_split($matches[1]) as $letter) {
            $col = $col * 26 + ord($letter) - 0x60;
        }

        return [$col, $row];
    }

    /**
     * Проверка корректности указания координат
     * @param string $coordinates
     * @return bool
     */
    public static function checkString(string $coordinates): bool
    {
        return (bool)preg_match(self::COORDINATES_STRING_REGEXP, $coordinates, $matches);
    }
}
