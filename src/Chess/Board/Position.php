<?php
/**
 * Этот файл создан в рамках тестового задания Local Internet
 * @author Anton Tyutin <anton@tyutin.ru>
 * @license MIT
 */

namespace LocalInternet\Chess\Board;


use LocalInternet\Chess\Piece\AbstractPiece;

/**
 * Положение фигуры на доске
 * @package LocalInternet\Chess\Board
 */
class Position
{
    /**
     * Доска
     * @var Board
     */
    private $board;
    /**
     * Фигура
     * @var AbstractPiece
     */
    private $piece;
    /**
     * Координаты в формате "b8"
     * @var string
     */
    private $coordinates;

    public function __construct(Board $board, AbstractPiece $piece, string $coordinates)
    {
        $this->board = $board;
        $this->piece = $piece;
        $this->coordinates = $coordinates;
    }

    /**
     * @return AbstractPiece
     */
    public function getPiece(): AbstractPiece
    {
        return $this->piece;
    }

    /**
     * @return string
     */
    public function getCoordinates(): string
    {
        return $this->coordinates;
    }

    public function __toString(): string
    {
        return $this->piece . $this->coordinates;
    }
}
