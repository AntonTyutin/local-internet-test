<?php
/**
 * Этот файл создан в рамках тестового задания Local Internet
 * @author Anton Tyutin <anton@tyutin.ru>
 * @license MIT
 */

namespace LocalInternet\Chess\Board\SerializeHandler;


use JMS\Serializer\Context;
use JMS\Serializer\Exception\RuntimeException;
use JMS\Serializer\GraphNavigator;
use JMS\Serializer\Handler\SubscribingHandlerInterface;
use JMS\Serializer\JsonDeserializationVisitor;
use JMS\Serializer\JsonSerializationVisitor;
use LocalInternet\Chess\Board\Board;
use LocalInternet\Chess\Board\Position;
use LocalInternet\Chess\Piece\AbstractPiece;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class BoardHandler implements SubscribingHandlerInterface
{

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @return array
     */
    public static function getSubscribingMethods()
    {
        return [
            [
                'direction' => GraphNavigator::DIRECTION_SERIALIZATION,
                'format' => 'json',
                'type' => Board::class,
                'method' => 'toJson',
            ],
            [
                'direction' => GraphNavigator::DIRECTION_DESERIALIZATION,
                'format' => 'json',
                'type' => Board::class,
                'method' => 'fromJson',
            ],
        ];
    }

    public function toJson(JsonSerializationVisitor $visitor, Board $board, array $type, Context $context)
    {
        return $visitor->visitArray([
            'width' => $board->getWidth(),
            'height' => $board->getHeight(),
            'positions' => array_map(
                function (Position $pos) { return implode(':', [$pos->getPiece(), $pos->getCoordinates()]); },
                $board->getPositions()
            ),
        ], $type, $context);
    }

    public function fromJson(JsonDeserializationVisitor $visitor, array $boardData, array $type, Context $context)
    {
        $board = new Board($boardData['width'], $boardData['height'], $this->eventDispatcher);
        $typesMap = $this->getPiecesDiscriminatorMap($context);

        foreach ($boardData['positions'] as $positionString) {
            list($pieceType, $coordinates) = explode(':', $positionString);
            if (!isset($typesMap[$pieceType])) {
                throw new RuntimeException(
                    'Ошибка восстановления состояния доски. На доске неизвестная фигура "' . $pieceType . '"'
                );
            }
            $pieceClassName = $typesMap[$pieceType];
            $board->addPiece(new $pieceClassName(), $coordinates);
        }

        return $board;
    }

    /**
     * @param Context $context
     * @return array
     */
    protected function getPiecesDiscriminatorMap(Context $context): array
    {
        return $context->getMetadataFactory()->getMetadataForClass(AbstractPiece::class)->discriminatorMap;
    }
}
