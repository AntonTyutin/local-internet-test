<?php
/**
 * Этот файл создан в рамках тестового задания Local Internet
 * @author Anton Tyutin <anton@tyutin.ru>
 * @license MIT
 */

namespace LocalInternet\Chess\EventDispatcher;


use LocalInternet\Chess\Board\Position;
use Symfony\Component\EventDispatcher\Event;

abstract class PositionEvent extends Event
{
    /**
     * @var Position
     */
    private $position;

    public function __construct(Position $position)
    {
        $this->position = $position;
    }

    /**
     * @return Position
     */
    public function getPosition(): Position
    {
        return $this->position;
    }
}
