<?php
/**
 * Этот файл создан в рамках тестового задания Local Internet
 * @author Anton Tyutin <anton@tyutin.ru>
 * @license MIT
 */

namespace LocalInternet\Chess\EventDispatcher;


use LocalInternet\Chess\Board\Position;

class PositionMoveEvent extends PositionEvent
{
    /**
     * @var Position
     */
    private $from;

    public function __construct(Position $to, Position $from)
    {
        $this->from = $from;

        parent::__construct($to);
    }

    /**
     * @return Position
     */
    public function getFrom(): Position
    {
        return $this->from;
    }
}
