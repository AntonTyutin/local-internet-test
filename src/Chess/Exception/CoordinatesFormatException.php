<?php
/**
 * Этот файл создан в рамках тестового задания Local Internet
 * @author Anton Tyutin <anton@tyutin.ru>
 * @license MIT
 */

namespace LocalInternet\Chess\Exception;

use Exception;

/**
 * Ошибка указания координат
 * @package LocalInternet\Chess\Exception
 */
class CoordinatesFormatException extends Exception
{
    public function __construct($message = 'Некорректно указаны координаты', $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
