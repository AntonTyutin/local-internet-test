<?php
/**
 * Этот файл создан в рамках тестового задания Local Internet
 * @author Anton Tyutin <anton@tyutin.ru>
 * @license MIT
 */

namespace LocalInternet\Chess\Exception;


use Exception;
use LocalInternet\Chess\Board\Board;

class EmptyCellException extends PlacementException
{
    public function __construct(Board $board, string $coordinates, Exception $previous = null)
    {
        parent::__construct($board, $coordinates, 'Указанная клетка ' . $coordinates . ' пуста.', $previous);
    }
}
