<?php
/**
 * Этот файл создан в рамках тестового задания Local Internet
 * @author Anton Tyutin <anton@tyutin.ru>
 * @license MIT
 */

namespace LocalInternet\Chess\Exception;


use Exception;
use LocalInternet\Chess\Board\Board;
use LocalInternet\Chess\Piece\AbstractPiece;

/**
 * Class PieceUnableMoveException
 * @package LocalInternet\Chess\Piece
 */
class InvalidMoveException extends Exception
{
    /**
     * @var Board
     */
    private $board;
    /**
     * @var AbstractPiece
     */
    private $piece;
    /**
     * @var string
     */
    private $from;
    /**
     * @var string
     */
    private $to;

    /**
     * @param Board $board
     * @param AbstractPiece $piece
     * @param string $from
     * @param string $to
     */
    public function __construct(Board $board, AbstractPiece $piece, string $from, string $to)
    {
        parent::__construct('Неправильный ход ' . $piece . $from . '—' . $to);

        $this->board = $board;
        $this->piece = $piece;
        $this->from = $from;
        $this->to = $to;
    }

    /**
     * @return Board
     */
    public function getBoard(): Board
    {
        return $this->board;
    }

    /**
     * @return AbstractPiece
     */
    public function getPiece(): AbstractPiece
    {
        return $this->piece;
    }

    /**
     * @return string
     */
    public function getFrom(): string
    {
        return $this->from;
    }

    /**
     * @return string
     */
    public function getTo(): string
    {
        return $this->to;
    }
}
