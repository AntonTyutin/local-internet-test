<?php
/**
 * Этот файл создан в рамках тестового задания Local Internet
 * @author Anton Tyutin <anton@tyutin.ru>
 * @license MIT
 */

namespace LocalInternet\Chess\Exception;


use Exception;
use LocalInternet\Chess\Board\Board;

/**
 * Ошибка размещения фигуры на доске
 * @package LocalInternet\Chess\Board
 */
abstract class PlacementException extends Exception
{
    /**
     * @var Board
     */
    private $board;

    private $coordinates;

    public function __construct(Board $board, string $coordinates, $message = '', Exception $previous = null)
    {
        parent::__construct($message, 0, $previous);
        $this->board = $board;
        $this->coordinates = $coordinates;
    }

    public function getPosition()
    {
        return $this->board->getPosition($this->coordinates);
    }
}
