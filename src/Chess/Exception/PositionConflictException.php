<?php
/**
 * Этот файл создан в рамках тестового задания Local Internet
 * @author Anton Tyutin <anton@tyutin.ru>
 * @license MIT
 */

namespace LocalInternet\Chess\Exception;


use Exception;
use LocalInternet\Chess\Board\Board;
use LocalInternet\Chess\Piece\AbstractPiece;

/**
 * Конфликт размещения двух фигур на доске
 * @package LocalInternet\Chess\Board
 */
class PositionConflictException extends PlacementException
{

    /**
     * @param Board $board
     * @param AbstractPiece $piece
     * @param string $coordinates
     * @param Exception $previous
     */
    public function __construct(Board $board, AbstractPiece $piece, string $coordinates, Exception $previous = null)
    {
        $message = 'Клетка ' . $coordinates . ' уже занята фигурой ' . $board->getPosition($coordinates);

        parent::__construct($board, $coordinates, $message, $previous);
    }
}
