<?php
/**
 * Этот файл создан в рамках тестового задания Local Internet
 * @author Anton Tyutin <anton@tyutin.ru>
 * @license MIT
 */

namespace LocalInternet\Chess\Piece;

/**
 * Шахматная фигура
 * @package LocalInternet\ChessTest\Piece
 */
abstract class AbstractPiece
{
    /**
     * Получение кода типа шахматной фигуры
     * @return string
     */
    abstract public function getType(): string;

    /**
     * Проверка доступности для фигуры такого хода
     * @param string $from
     * @param string $to
     * @return bool
     */
    abstract public function isAbleToMove(string $from, string $to): bool;

    public function __toString(): string
    {
        return $this->getType();
    }
}
