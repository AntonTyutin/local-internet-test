<?php
/**
 * Этот файл создан в рамках тестового задания Local Internet
 * @author Anton Tyutin <anton@tyutin.ru>
 * @license MIT
 */

namespace LocalInternet\Chess\Piece;


use LocalInternet\Chess\Board\CoordinatesConverter;

class Pawn extends AbstractPiece
{
    /**
     * {@inheritdoc}
     */
    public function getType(): string
    {
        return 'p';
    }

    /**
     * {@inheritdoc}
     */
    public function isAbleToMove(string $from, string $to): bool
    {
        list($fromCol, $fromRow) = CoordinatesConverter::fromString($from);
        list($toCol, $toRow) = CoordinatesConverter::fromString($to);

        return $fromCol == $toCol && 1 == $toRow - $fromRow;
    }
}
