<?php
/**
 * Этот файл создан в рамках тестового задания Local Internet
 * @author Anton Tyutin <anton@tyutin.ru>
 * @license MIT
 */


use JMS\Serializer\Handler\HandlerRegistry;
use JMS\Serializer\SerializerBuilder;
use LocalInternet\Chess\Board\Board;
use LocalInternet\Chess\Board\Position;
use LocalInternet\Chess\Board\SerializeHandler\BoardHandler;
use LocalInternet\Chess\Piece\AbstractPiece;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;


class BoardSerializationTest extends PHPUnit_Framework_TestCase
{

    public function testSerialize()
    {
        $eventDispatcher = $this->createEventDispatcherMock();
        $board = new Board(5, 5, $eventDispatcher);
        $board->addPiece($this->createPieceMock('A'), 'c2');
        $board->addPiece($this->createPieceMock('B'), 'e1');
        $board->addPiece($this->createPieceMock('C'), 'a5');

        $serializer = $this->createSerializerService($eventDispatcher);

        $this->assertJsonStringEqualsJsonString(
            '{"width": 5, "height": 5, "positions": ["A:c2", "B:e1", "C:a5"]}',
            $serializer->serialize($board, 'json')
        );
    }

    public function testDeserialize()
    {
        $eventDispatcher = $this->createEventDispatcherMock();

        $serializer = $this->createSerializerService($eventDispatcher);

        $serializedRepresentation = '{"width": 5, "height": 7, "positions": ["A:c2", "B:e1", "C:a5"]}';

        /** @var Board $board */
        $board = $serializer->deserialize($serializedRepresentation, Board::class, 'json');

        $this->assertInstanceOf(Board::class, $board);
        $this->assertEquals(5, $board->getWidth());
        $this->assertEquals(7, $board->getHeight());
        $this->assertCount(3, $board->getPositions());
        $this->assertInstanceOf(Position::class, $board->getPositions()[0]);
        $this->assertEquals('c2', $board->getPositions()[0]->getCoordinates());
        $this->assertInstanceOf(AbstractPiece::class, $board->getPositions()[0]->getPiece());
        $this->assertEquals('A', $board->getPositions()[0]->getPiece()->getType());
    }


    /**
     * @return PHPUnit_Framework_MockObject_MockObject|EventDispatcherInterface
     */
    private function createEventDispatcherMock(): PHPUnit_Framework_MockObject_MockObject
    {
        return $this->getMockForAbstractClass(EventDispatcherInterface::class);
    }

    /**
     * @param string $type
     * @return AbstractPiece|PHPUnit_Framework_MockObject_MockObject
     */
    private function createPieceMock(string $type = 'F'): PHPUnit_Framework_MockObject_MockObject
    {
        $mock = $this->getMockForAbstractClass(AbstractPiece::class);
        $mock->expects($this->any())->method('getType')->willReturn($type);

        return $mock;
    }

    private function createPieceClass($type)
    {
        $className = uniqid($type);

        eval('class ' . $className . ' extends LocalInternet\Chess\Piece\AbstractPiece {
            public function getType(): string { return "' . $type . '"; }
            public function isAbleToMove(string $from, string $to): bool { return true; }
        }');

        return $className;
    }

    /**
     * @param $eventDispatcher
     * @return \JMS\Serializer\Serializer
     */
    private function createSerializerService($eventDispatcher): \JMS\Serializer\Serializer
    {
        return SerializerBuilder::create()
            ->addMetadataDir(__DIR__ . '/../config/serializer')
            ->configureHandlers(function (HandlerRegistry $registry) use ($eventDispatcher) {
                /** @var \JMS\Serializer\Handler\SubscribingHandlerInterface|PHPUnit_Framework_MockObject_MockObject $boardHandler */
                $boardHandler = $this->getMockBuilder(BoardHandler::class)
                    ->setConstructorArgs([$eventDispatcher])
                    ->setMethods(['getPiecesDiscriminatorMap'])
                    ->getMock();

                $boardHandler->expects($this->any())->method('getPiecesDiscriminatorMap')->willReturn([
                    'A' => $this->createPieceClass('A'),
                    'B' => $this->createPieceClass('B'),
                    'C' => $this->createPieceClass('C'),
                ]);
                $registry->registerSubscribingHandler($boardHandler);
            })
            ->build();
    }
}
