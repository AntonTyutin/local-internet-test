<?php
/**
 * Этот файл создан в рамках тестового задания Local Internet
 * @author Anton Tyutin <anton@tyutin.ru>
 * @license MIT
 */


use LocalInternet\Chess\Board\Board;
use LocalInternet\Chess\Board\Position;
use LocalInternet\Chess\Exception\CoordinatesFormatException;
use LocalInternet\Chess\Exception\InvalidMoveException;
use LocalInternet\Chess\Piece\AbstractPiece;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;


class BoardTest extends PHPUnit_Framework_TestCase
{
    public function testAddPiece()
    {
        $eventDispatcher = $this->createEventDispatcherMock();
        $board = new Board(5, 5, $eventDispatcher);
        $eventDispatcher->expects($this->once())->method('dispatch')->with('chess_board.position.add');

        $board->addPiece($this->createPieceMock(), 'a1');
        $this->assertTrue($board->hasPieceAt('a1'));
    }

    /**
     * @dataProvider provideInvalidCoordinates
     * @param string $coordinates
     */
    public function testAddPieceInvalidCoordinates(string $coordinates)
    {
        $this->expectException(CoordinatesFormatException::class);

        $eventDispatcher = $this->createEventDispatcherMock();
        $board = new Board(5, 5, $eventDispatcher);
        $eventDispatcher->expects($this->never())->method('dispatch')->with('chess_board.position.add');

        $board->addPiece($this->createPieceMock(), $coordinates);
    }

    public function testRemovePiece()
    {
        $eventDispatcher = $this->createEventDispatcherMock();
        $board = new Board(5, 5, $eventDispatcher);
        $board->addPiece($this->createPieceMock(), 'a1');

        $eventDispatcher->expects($this->once())->method('dispatch')->with('chess_board.position.remove');
        $pos = $board->removePiece('a1');

        $this->assertFalse($board->hasPieceAt('a1'));
        $this->assertInstanceOf(Position::class, $pos);
        $this->assertEquals('a1', $pos->getCoordinates());
    }

    /**
     * @dataProvider provideInvalidCoordinates
     * @param string $coordinates
     */
    public function testRemovePieceInvalidCoordinates(string $coordinates)
    {
        $this->expectException(CoordinatesFormatException::class);

        $eventDispatcher = $this->createEventDispatcherMock();
        $board = new Board(5, 5, $eventDispatcher);
        $eventDispatcher->expects($this->never())->method('dispatch')->with('chess_board.position.remove');

        $board->removePiece($coordinates);
    }

    public function testMovePiece()
    {
        $eventDispatcher = $this->createEventDispatcherMock();
        $board = new Board(5, 5, $eventDispatcher);
        $piece = $this->createPieceMock();
        $piece->expects($this->once())->method('isAbleToMove')->willReturn(true);
        $board->addPiece($piece, 'a1');

        $eventDispatcher->expects($this->once())->method('dispatch')->with('chess_board.position.move');
        $board->movePiece('a1', 'c5');
        $this->assertFalse($board->hasPieceAt('a1'));
        $this->assertTrue($board->hasPieceAt('c5'));
    }

    public function testMovePieceInvalidMove()
    {
        $this->expectException(InvalidMoveException::class);

        $eventDispatcher = $this->createEventDispatcherMock();
        $board = new Board(5, 5, $eventDispatcher);
        $piece = $this->createPieceMock();
        $piece->expects($this->once())->method('isAbleToMove')->willReturn(false);
        $board->addPiece($piece, 'a1');

        $board->movePiece('a1', 'c5');
    }

    public function provideInvalidCoordinates()
    {
        return array_map(function ($val) { return [$val]; }, [
            'f9', 'a0'
        ]);
    }

    /**
     * @return PHPUnit_Framework_MockObject_MockObject|EventDispatcherInterface
     */
    private function createEventDispatcherMock(): PHPUnit_Framework_MockObject_MockObject
    {
        return $this->getMockForAbstractClass(EventDispatcherInterface::class);
    }

    /**
     * @return PHPUnit_Framework_MockObject_MockObject|AbstractPiece
     */
    private function createPieceMock(): PHPUnit_Framework_MockObject_MockObject
    {
        $mock = $this->getMockForAbstractClass(AbstractPiece::class);
        $mock->expects($this->any())->method('getType')->willReturn('F');

        return $mock;
    }
}
