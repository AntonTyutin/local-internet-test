<?php
/**
 * Этот файл создан в рамках тестового задания Local Internet
 * @author Anton Tyutin <anton@tyutin.ru>
 * @license MIT
 */


use LocalInternet\Chess\Board\CoordinatesConverter;
use LocalInternet\Chess\Exception\CoordinatesFormatException;


class CoordinatesConverterTest extends PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider provideConvertCoordinatesFromString
     * @param string $string
     * @param array $expectedResult
     */
    public function testConvertCoordinatesFromString($string, $expectedResult)
    {
        $this->assertEquals($expectedResult, CoordinatesConverter::fromString($string));
    }

    public function provideConvertCoordinatesFromString()
    {
        return [
            ["a5", [1,5]],
            ["a10", [1,10]],
            ["z10", [26,10]],
            ["aa10", [27,10]],
        ];
    }

    /**
     * @dataProvider provideConvertCoordinatesToString
     * @param array $coordinates
     * @param string $expectedResult
     */
    public function testConvertCoordinatesToString($coordinates, $expectedResult)
    {
        $this->assertEquals($expectedResult, CoordinatesConverter::toString($coordinates));
    }

    public function provideConvertCoordinatesToString()
    {
        return [
            [[1,5], "a5"],
            [[1,10], "a10"],
            [[26,10], "z10"],
            [[27,10], "aa10"],
            [[731,33], "abc33"],
        ];
    }

    /**
     * @dataProvider provideConvertInvalidCoordinates
     * @param string|array $invalid
     */
    public function testConvertInvalidCoordinates($invalid)
    {
        $this->expectException(CoordinatesFormatException::class);
        if (is_array($invalid)) {
            CoordinatesConverter::toString($invalid);
        } else {
            CoordinatesConverter::fromString($invalid);
        }
    }

    public function provideConvertInvalidCoordinates()
    {
        return array_map(function ($val) { return [$val]; }, [
            'a0', '1', 'a', 'A1', '.1',
            [0, 1], [1, 0], [-1, 1], [1, -1], [1], ['', 1], [0.5, 1],
        ]);
    }
}
